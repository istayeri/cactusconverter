package assets.stayer.view

import assets.stayer.app.Quarter
import assets.stayer.app.Styles
import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Pos
import javafx.scene.input.DataFormat
import javafx.scene.input.TransferMode
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import tornadofx.*
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

class MainView : View("Cactus Converter") {

    private val filePath = SimpleStringProperty()
    private var realFilePath = ""
    private val progressProp = SimpleDoubleProperty()
    private lateinit var lblDone: javafx.scene.control.Label

    override val root = vbox {
        paddingAll = 20
        alignment = Pos.TOP_CENTER
        label("Cactus Converter") {
            addClass(Styles.heading)
        }
        label("Drag & Drop the file here:") {
            vboxConstraints {
                marginTop = 20.0
            }
        }
        textfield(filePath) {
            vboxConstraints {
                marginBottom = 20.0
            }
            setOnDragOver {
                if (it.gestureSource != this
                        && it.dragboard.hasFiles()) {
                    it.acceptTransferModes(TransferMode.COPY)
                }
                it.consume()
            }
            setOnDragDropped {
                val db = it.dragboard
                var success = false
                val files: List<File>? = db.getContent(DataFormat.FILES) as ArrayList<File>?

                if (files != null) {
                    val file = files[0]
                    this.text = file.absolutePath
                    success = true
                }

                it.isDropCompleted = success
                it.consume()
            }
        }
        progressbar(progressProp) {
            vboxConstraints {
                marginBottom = 20.0
            }
            prefWidth = 500.0
            progress = 0.0
        }
        button("Convert!") {
            action {
                progressProp.value = 0.0
                realFilePath =
                        if (filePath.value.contains("file://")) {
                            filePath.value.replace("file://", "")
                        } else {
                            filePath.value
                        }
                parseFile()
                lblDone.isVisible = false
            }
        }
        lblDone = label("DONE!") {
            addClass(Styles.status)
            vboxConstraints {
                marginTop = 30.0
            }
            isVisible = false
        }
    }

    private fun parseFile() {

        val excelFile = FileInputStream(File(realFilePath))

        val workBook = XSSFWorkbook(excelFile)

        val sheet = workBook.getSheetAt(0)
        val rows = sheet.iterator().withIndex()
        val numberOfRows = sheet.physicalNumberOfRows

        val quarters = mutableListOf<Quarter>()

        runAsync {

            while (rows.hasNext()) {

                val currentRow = rows.next()
                val cellsInRow = currentRow.value.iterator()

                if (currentRow.index == 0) {
                    continue
                }

                var quarterTmp: Quarter
                var forestry = ""
                var uchLesVo = ""
                var area = ""
                var idQuarter = -1.0
                var videl: Double

                while (cellsInRow.hasNext()) {
                    val currentCell = cellsInRow.next()
                    val sCurrCellValue =
                            if (currentCell.cellTypeEnum === CellType.NUMERIC) {
                                currentCell.numericCellValue.toString()
                            } else if (currentCell.cellTypeEnum === CellType.STRING) {
                                currentCell.stringCellValue.toString()
                            } else {
                                continue
                            }

                    when (currentCell.columnIndex) {
                        0 -> forestry = sCurrCellValue
                        1 -> uchLesVo = sCurrCellValue
                        2 -> area = sCurrCellValue
                        3 -> idQuarter = sCurrCellValue.toDouble()
                        4 -> {
                            videl = sCurrCellValue.toDouble()
                            quarterTmp = Quarter(forestry, uchLesVo, area, idQuarter)
                            if (quarters.size == 0 || !quarters[quarters.size - 1].match(quarterTmp)) {
                                quarterTmp.videls.add(videl)
                                quarters.add(quarterTmp)
                            } else {
                                quarters[quarters.size - 1].videls.add(videl)
                            }
                        }
                    }
                }

                runLater {
                    val value = (currentRow.index.toDouble() / (numberOfRows - 1))
                    progressProp.value = value
                }
            }

            runLater { createDocument(quarters.toList()) }
        }
    }

    private fun createDocument(quarters: List<Quarter>) {

        val workbook = XSSFWorkbook()

        val sheet = workbook.createSheet("CactusDocument")
        val firstRow = sheet.createRow(0)
        firstRow.createCell(0).setCellValue("les_vo")
        firstRow.createCell(1).setCellValue("uch_les_vo")
        firstRow.createCell(2).setCellValue("uchastok")
        firstRow.createCell(3).setCellValue("kv")
        firstRow.createCell(4).setCellValue("videl")

        var rowIdx = 1
        for (quarter in quarters) {
            val row = sheet.createRow(rowIdx++)
            row.createCell(0).setCellValue(quarter.forestry)
            row.createCell(1).setCellValue(quarter.uchLesVo)
            row.createCell(2).setCellValue(quarter.area)
            row.createCell(3).setCellValue(quarter.id)
            row.createCell(4).setCellValue(quarter.getVidelsAsString())
        }

        val outPath = realFilePath.replace(".xlsx", "_new.xlsx")
        val fileOut = FileOutputStream(outPath)
        workbook.write(fileOut)
        fileOut.close()
        workbook.close()

        lblDone.isVisible = true
    }
}
