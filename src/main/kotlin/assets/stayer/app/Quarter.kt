package assets.stayer.app

class Quarter(
        var forestry: String,
        var uchLesVo: String,
        var area: String,
        var id: Double) {

    var videls = mutableListOf<Double>()

    fun match(quarter: Quarter): Boolean {
        return (quarter.forestry == forestry
                && quarter.uchLesVo == uchLesVo
                && quarter.area == area
                && quarter.id == id)
    }

    fun getVidelsAsString(): String {
        val sb = StringBuilder()

        for (videl in videls) {
            sb.append("${videl.toInt()},")
        }

        sb.deleteCharAt(sb.length - 1)
        return sb.toString()
    }
}