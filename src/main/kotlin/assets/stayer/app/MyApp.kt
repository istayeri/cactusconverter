package assets.stayer.app

import assets.stayer.view.MainView
import javafx.application.Application
import javafx.stage.Stage
import tornadofx.App

class MyApp: App(MainView::class, Styles::class) {
    override fun start(stage: Stage) {
        stage.minWidth = 600.0
        stage.minHeight = 400.0
        super.start(stage)
    }
}

fun main(args: Array<String>) {
    Application.launch(MyApp::class.java, *args)
}